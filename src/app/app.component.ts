import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  habit: string = '';
  email: string = '';
  name: string = '';
  age: string = '';
  height: string = '';
  gender: string = '';

  pickedHabit: boolean;
  completeSignup: boolean;

  itemRef: AngularFireObject<any>;
  itemRef2: AngularFireObject<any>;

  itemcount: any;

  constructor(afDb: AngularFireDatabase) {
    this.itemRef = afDb.object('supporters');

    this.itemRef.snapshotChanges().subscribe(action => {
      if(!action.payload.val().length){
        this.itemcount = 0
      }else{
        this.itemcount = action.payload.val().length
        this.itemRef2 = afDb.object('supporters/' + this.itemcount);
      }
    });

  }

  setHabit() {
    if(this.habit && this.email){
      this.pickedHabit = true;
    }else{
      console.log("NO HABIT/EMAIL")
    }
  }

  completeSetup() {
    if(this.habit && this.email && this.name && this.age && this.height && this.gender){
      this.completeSignup = true;

      this.itemRef2.set({
        habit: this.habit,
        email: this.email,
        name: this.name,
        age: this.age,
        height: this.height,
        gender: this.gender
      });

      console.log("Signup Allowed")
    }else{
      console.log("Missing Details")
    }
  }

}
